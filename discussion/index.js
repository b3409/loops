//let count = 5;
//while loop
//syntax: while(expression/condition){

//statement/ block of codes 
//}
// Do-while loop
// syntax: 
//        do {
// statement
//}   while(expression/condition);
// for loop: 
for( let count = 0; count <= 20; count++){
console.log(count);
};

let myString = "alex";

console.log(myString.length);
// result - 4

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

// total index= length - 1

for (let x = 0; x < myString.length; x++){
     console.log(myString[x])   
};

//continue break concept
















